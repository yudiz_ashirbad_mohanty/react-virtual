import React,{useState,useEffect} from "react";
import faker from "faker";
import { CellMeasurer } from "react-virtualized/dist/commonjs/CellMeasurer";
import { CellMeasurerCache } from "react-virtualized/dist/commonjs/CellMeasurer";
import AutoSizer from "react-virtualized/dist/commonjs/AutoSizer";
import List from "react-virtualized/dist/commonjs/List";

import './App.css'
export default function App() {
  const cache = React.useRef(
    new CellMeasurerCache({
      fixedWidth: true,
      defaultHeight: 200,
    })
  );
  const [product, setproduct] =useState([]);


 useEffect(() => {
    setproduct(
      [...Array(1000).keys()].map((key) => {
        return {
          id: key,
          name: faker.commerce.product(),
          price: faker.commerce.price(2, 22, 1, "$"),
          image:faker.image.business(200, 200, true),
          Description:faker.lorem.lines(),
        };
      })
    );
  }, []);

  return (
    <div>
<div className="subhadding">
<h2 >REACT VIRTUAL</h2>
<h2 >CARDS</h2>
</div>
      <div className="maincard" style={{ width: "100%", height: "100vh" }}>
        <AutoSizer>
          {({ width, height }) => (
            <List
              width={width}
              height={height}
              rowHeight={cache.current.rowHeight}
              deferredMeasurementCache={cache.current}
              rowCount={product.length}
              rowRenderer={({ key, index, parent }) => {
                const data = product[index];

                return (
                  
                  <CellMeasurer
                    key={key}
                    cache={cache.current}
                    parent={parent}
                    columnIndex={0}
                    rowIndex={index}
                  >
                  <div>
                    <div className="card" >
                     <div className="cardsub">
                     <h2 className="name" >{data.name}</h2>
                   <div>
                    <img className="image" src={data.image} alt=''></img>
                      <p className="price">{data.price}</p>
                      <p className="description">{data.Description}</p>
                   </div>
                     </div>
                    <div>
                  
                    </div>
                    </div>
                    </div>
                  </CellMeasurer>
                );
              }}
            />
          )}
        </AutoSizer>
      </div>
    </div>
  );
}